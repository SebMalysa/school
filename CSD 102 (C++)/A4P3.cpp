//Program Name: A4P3.cpp
//Programmer: Sebastian Malysa
//Purpose: User name and password program
//Date Written: 09/03/2016
//Date Last Modified: 12/03/2016

#include <iostream>
//#include <process.h>
//#include <iomanip>
#include <conio.h>
#include <string>
//#include <stdio.h>
//#include <ctype.h>
#include <windows.h> //required to implement screen coordinates
#include <iomanip>

using namespace std;

// gotoxy function

void gotoxy(int x, int y)
{
	COORD coord;
	coord.X = x;  //column coordinate
	coord.Y = y;	//row coordinate
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

// clear screen function

void clrscr(int x, int y)
{
	COORD                       coordScreen = { x, y };
	DWORD                       cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO  csbi;
	DWORD                       dwConSize;
	HANDLE                      hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
	FillConsoleOutputCharacter(hConsole, TEXT(' '),
		dwConSize, coordScreen, &cCharsWritten);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	FillConsoleOutputAttribute(hConsole, csbi.wAttributes,
		dwConSize, coordScreen, &cCharsWritten);
	SetConsoleCursorPosition(hConsole, coordScreen);
}

void userNameInput(string userNames[10], char userName[11])
{
	short unsigned int index = 0;
	
	// Setup prompts 

	clrscr(0, 0);
	system("color 07");
	gotoxy(23, 2);
	cout << "Are you Worthy for the Answer";
	gotoxy(20, 3);
	cout << "To Life, the Universe and Everything?";
	gotoxy(5, 4);
	cout << "---------------------------------------------------------------------";
	gotoxy(34, 6);
	cout << "Show Us!";
	gotoxy(20, 11);
	cout << "User Name:";
	gotoxy(21, 13);
	cout << "Password:";
	gotoxy(5, 20);
	cout << "---------------------------------------------------------------------";
	gotoxy(31, 11);

	while (true)
	{
		userName[index] = _getche();

		if ((index == 0) && (userName[index] == '\b'))
		{
			cout << " ";
			index--;
		}

		else if (userName[index] == '\b')
		{
			cout << " " << '\b';
			index -= 2; 
		}

		else if (userName[index] == '\r')
		{
			userName[index] = '\0';
			break;
		}

		else if (index == 10)
		{
			cout << '\b' << " " << '\b' << '\a';
			index--;
		}

		index++;
	}
}

void passwordInput(string passwords[10], char password[11])
{
	short unsigned int index = 0;

	gotoxy(31, 13);

	while (true)
	{
		password[index] = _getch();

		if ((index == 0) && (password[index] == '\b'))
		{
			cout << " " << '\b';
			index--;
		}

		else if (password[index] == '\b')
		{
			cout << '\b' << " " << '\b';
			index -= 2;
		}

		else if (password[index] == '\r')
		{
			password[index] = '\0';
			break;
		}

		else if (index == 10)
		{
			cout << " " << '\b' << '\a';
			index--;
		}

		else
		{
			_putch('*');
		}

		index++;
	}
}

void match(string userNames[10], string passwords[10], char userName[11], char password[11], short unsigned int &attempt)
{
	char again;

	for (short unsigned int i = 0; i < 10; i++)
	{
		if ((userName == userNames[i]) && (password == passwords[i]))
		{
			for (short unsigned int x = 0; x < 2; x++)
			{
				cout << '\a';
				system("color 2F");
				Sleep(1000);

				cout << '\a';
				system("color 02");
				Sleep(1000);
			}

			clrscr(0, 0);
			gotoxy(26, 8);
			cout << "Your worth is undeniable";
			gotoxy(9, 10);
			cout << "The ultimate answer to life, the universe and everything is...";
			gotoxy(37, 12);
			cout << "42";

			gotoxy(11, 15);
			cout << "Enter another user name and password combination? (y/n):";
			
			while (true)
			{
				gotoxy(68, 15);
				again = _getch();
				again = tolower(again);

				if (again == 'y')
				{
					attempt = 3;
					break;
				}

				else if (again == 'n')
				{
					attempt = 0;
					break;
				}

				else
				{
					gotoxy(30, 17);
					cout << "Please use y or n.";
				}
			}
			break;
		}

		else if (i == 9)
		{
			attempt--; 

			if (attempt == 0)
			{
				for (short unsigned int y = 0; y < 2; y++)
				{
					cout << '\a';
					system("color 4F");
					Sleep(1000);

					cout << '\a';
					system("color 0C");
					Sleep(1000);
				}

				clrscr(0, 0);
				gotoxy(25, 8);
				cout << "You're part of the solution";
				gotoxy(30, 9);
				cout << "Not the problem";
				gotoxy(33, 11);
				cout << "Sincerely";
				gotoxy(33, 12);
				cout << "The Mice";

				gotoxy(27, 14);
				cout << "You are now locked out!";
				_getch();
			}

			else
			{
				gotoxy(18, 16);
				cout << "Invalid username and password combination!";
				gotoxy(27, 17);
				cout << "You have " << attempt << " attempts left.";

				gotoxy(24, 22);
				cout << "Press any key to try again...";
				_getch();

				clrscr(1, 16);
			}
		}
	}
}

int main()
{
	string userNames[10] = { "ArthurDent", "FPrefect", "Trillian", "Zaphod", "Marvin", "Almighty", 
							"Vogon", "Dolphins", "Babel", "Mice" };

	string passwords[10] = { "adent", "ford01", "trishaM", "beeblebrox", "001101", "BoB",
							"vogsphere", "2nd", "fish", "1st" };
	char userName[11];
	char password[11];
	short unsigned int attempt = 3;

	while (attempt > 0)
	{
		userNameInput(userNames, userName);
		passwordInput(passwords, password);
		match(userNames, passwords, userName, password, attempt);
	}

	return 0;
}