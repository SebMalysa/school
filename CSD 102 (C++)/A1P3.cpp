//Program Name: A1P3.cpp
//Programmer: Sebastian Malysa
//Purpose: To calculate compounding interests for money in a savings account
//Date Written: 17/01/2016
//Date Last Modified: 24/01/2016

#include <iostream>
#include <string>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
	// Loop to check if user wants to execute the program again

	char again = 'y';
	while (again == 'y')
	{
		//Variable declarations 

		float principle = 0.0;
		float interestRate = 0.0;
		unsigned short int compound = 0;
		float interest = 0.0;
		float savings = 0.0;

		//Gather user input 

		cout << "\n\n\t\tPrinciple balance: $ ";
		cin >> principle;

		cout << "\n\t\tInterest rate: ";
		cin >> interestRate;

		cout << "\n\t\tTimes interest is compounded: ";
		cin >> compound;

		//Calculate the interest and the resulting amount in the savings account

		interest = principle * pow(1 + ((interestRate / 100) / compound), compound) - principle;
		savings = principle + interest;

		//Output results 

		cout << setprecision(2) << fixed;
		cout << endl;
		
		cout << setw(16) << " " << setw(20) << left << "Interest Rate: " 
			<< setw(5) << " " << setw(15) << right << interestRate << "%" << endl;
		
		cout << setw(16) << " " << setw(20) << left << "Times Compounded: " 
			<< setw(5) << " " << setw(15) << right << compound << endl;
		
		cout << setw(16) << " " << setw(20) << left << "Principle: " 
			<< setw(5) << "$ " << setw(15) << right << principle << endl;
		
		cout << setw(16) << " " << setw(20) << left << "Interest: " 
			<< setw(5) << "$ " << setw(15) << right << interest << endl;
		
		cout << setw(16) << " " << setw(20) << left << "Amount in Savings: " 
			<< setw(5) << "$ " << setw(15) << right << savings << endl;

		cout << "\n\n\t\tWould you like to run the program again? (y/n): ";
		cin >> again;
	}

	cin.ignore();
	cout << "\n\n\n\n\t\tPress any key to continue ";
	cin.get();

	return 0;
}