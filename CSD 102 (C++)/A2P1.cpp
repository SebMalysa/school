//Program Name: A2P1.cpp
//Programmer: Sebastian Malysa
//Purpose: Yearly rainfall calculator that displays total yearly rainfall and
//		   the average, highest, lowest monthly rainfall amounts
//Date Written: 28/01/2016
//Date Last Modified: 04/07/2016

#include <iostream>
//#include <process.h>
//#include <iomanip>
#include <conio.h>
#include <string>
//#include <stdio.h>
//#include <ctype.h>
#include <windows.h> //required to implement screen coordinates
#include <iomanip>

using namespace std;

// gotoxy function

void gotoxy(int x, int y)
{
	COORD coord;
	coord.X = x;  //column coordinate
	coord.Y = y;	//row coordinate
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

// clear screen function

void clrscr(int x, int y)
{
	COORD                       coordScreen = { x, y };
	DWORD                       cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO  csbi;
	DWORD                       dwConSize;
	HANDLE                      hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
	FillConsoleOutputCharacter(hConsole, TEXT(' '),
		dwConSize, coordScreen, &cCharsWritten);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	FillConsoleOutputAttribute(hConsole, csbi.wAttributes,
		dwConSize, coordScreen, &cCharsWritten);
	SetConsoleCursorPosition(hConsole, coordScreen);
}

//function that gathers monthly rainfall from user 

void input_rainfall_data(string months[12], unsigned short int monthlyRain[12])
{
	
	//local variable as y-coordinate control
	
	unsigned short int yControl = 3;

	//set up program titles

	clrscr(0, 0);
	gotoxy(30, 2);
	cout << "Monthly Rainfall" << endl;

	//monthly rainfall prompt loop

	for (unsigned short int i = 0; i < 12; i++)
	{
		gotoxy(31, yControl);
		cout << months[i] + ":" << endl; 
		yControl++;
	}
	
	//re-initialize yControl back to 4 for the cin loop
	
	yControl = 3;
	
	//monthly rainfall user input prompt loop 

	for (unsigned short int i = 0; i < 12; i++)
	{
		gotoxy(42, yControl);
		cin >> monthlyRain[i];
		yControl++;
	}
}

//function for determining the min and max monthly rainfall 
//also fills in an array for with the index of all matching min and max months 

void determine_min_max_rainfall(unsigned short int monthlyRain[12], unsigned short int &minRain, 
	unsigned short int &maxRain, unsigned short minMonths[12], unsigned short int maxMonths[12], 
	unsigned short int &minControl, unsigned short int &maxControl)
{
	//Variable initialization  

	monthlyRain[12];
	minRain = 0;
	maxRain = 0;
	minRain = monthlyRain[0];
	maxRain = monthlyRain[0];
	minControl = 0; 
	maxControl = 0;

	//loop to find the min and max rainfall 

	for (unsigned short int i = 0; i < 12; i++)
	{
		if (monthlyRain[i] < minRain)
			minRain = monthlyRain[i];
		
		else if (monthlyRain[i] > maxRain)
			maxRain = monthlyRain[i];
	}

	//loop to find the element reference of each occurrence of the min 
	//and max rainfall in the monthlyRain array

	for (unsigned short int i = 0; i < 12; i++)
	{
		if (monthlyRain[i] == maxRain)
		{
			maxMonths[maxControl] = i;
			maxControl++;
		}

		else if (monthlyRain[i] == minRain)
		{
			minMonths[minControl] = i;
			minControl++;
		}
	}

}

//function that calculates the total and average rainfall 

void calculate_total_and_average_rainfall(unsigned short int monthlyRain[12], 
	unsigned short int &totalRain, float &averageRain)
{
	//loop to calculate the total rainfall

	for (unsigned short int i = 0; i < 12; i++)
	{
		totalRain += monthlyRain[i];
	}

	//average rainfall calculation

	averageRain = totalRain / 12.0;
}

//function for data output 

void output_rainfall_report(string months[12], unsigned short int monthlyRain[12], 
	unsigned short int minRain, unsigned short int maxRain, unsigned short int minMonths[12], 
	unsigned short int maxMonths[12], unsigned short int minControl, unsigned short int maxControl, 
	unsigned short int totalRain, float averageRain)
{
	//output results title
	gotoxy(33, 16);
	cout << "Results" << endl;

	//output total and average rainfall

	gotoxy(13, 18);
	cout << "Total Rainfall: " << setprecision(1) << fixed << totalRain << " mm" << endl;
	gotoxy(13, 19);
	cout << "Average Rainfall: " << averageRain << " mm" << endl;
	
	//output for min and max months with a check to make sure all months do not equal the min/max

	if (minRain == maxRain)
	{
		gotoxy(13, 20);
		cout << "All months had the same rainfall this year." << endl;
	}

	else
	{
		gotoxy(13, 20);
		cout << "Month(s) with highest rainfall are: ";
		
		for (unsigned short int i = 0; i < maxControl; i++)
		{
			cout << months[maxMonths[i]];
			if (i < maxControl - 1)
			{
				cout << ", ";
			}
		}

		gotoxy(13, 21);
		cout << "Month(s) with lowest rainfall are: ";
		
		for (unsigned short int i = 0; i < minControl; i++)
		{
			cout << months[minMonths[i]];
			if (i < minControl - 1)
			{
				cout << ", ";
			}
		}
		
	}

}

int main()
{
	//variable declarations (constants/variables that do not need to be refreshed)
	string months[] = { "January", "February", "March", "April", "May", "June", "July",
		"August", "September", "October", "November", "December" };
	char again = 'y';

	//loop that re-execute the whole program based on user input
	
	while (again == 'y')
	{
		//variable declarations (variables that change/need to be refreshed upon re-execution)

		unsigned short int monthlyRain[12];
		unsigned short int minRain;
		unsigned short int maxRain;
		unsigned short int minMonths[12];
		unsigned short int maxMonths[12];
		unsigned short int minControl;
		unsigned short int maxControl;
		unsigned short int totalRain = 0;
		float averageRain = 0.0;
		
		//function calls 
		
		input_rainfall_data(months, monthlyRain);
		determine_min_max_rainfall(monthlyRain, minRain, maxRain, minMonths, maxMonths, minControl, maxControl);
		calculate_total_and_average_rainfall(monthlyRain, totalRain, averageRain);
		output_rainfall_report(months, monthlyRain, minRain, maxRain, minMonths, maxMonths, minControl, maxControl, totalRain, averageRain);
		
		//check if the user would like to re-execute the program
		
		gotoxy(13, 22);
		cout << "Calculate rainfall for another year? (y/n):" << endl;
		gotoxy(57, 22);
		again = _getch();
		again = tolower(again);

	}

	cin.ignore();
	gotoxy(45, 24);
	cout << "Press any key to continue...";
	_getch();

	return 0;

}

