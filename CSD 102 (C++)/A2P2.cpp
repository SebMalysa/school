//Program Name: A2P2.cpp
//Programmer: Sebastian Malysa
//Purpose: Simulate a vending machine 
//Date Written: 28/01/2016
//Date Last Modified: 04/07/2016

#include <iostream>
//#include <process.h>
//#include <iomanip>
#include <conio.h>
#include <string>
//#include <stdio.h>
//#include <ctype.h>
#include <windows.h> //required to implement screen coordinates
#include <iomanip>

using namespace std;

// gotoxy function

void gotoxy(int x, int y)
{
	COORD coord;
	coord.X = x;  //column coordinate
	coord.Y = y;	//row coordinate
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

// clear screen function

void clrscr(int x, int y)
{
	COORD                       coordScreen = { x, y };
	DWORD                       cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO  csbi;
	DWORD                       dwConSize;
	HANDLE                      hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
	FillConsoleOutputCharacter(hConsole, TEXT(' '),
		dwConSize, coordScreen, &cCharsWritten);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	FillConsoleOutputAttribute(hConsole, csbi.wAttributes,
		dwConSize, coordScreen, &cCharsWritten);
	SetConsoleCursorPosition(hConsole, coordScreen);
}

//function that welcomes a customer 

void greetings()
{
	clrscr(0, 0);
	gotoxy(30, 8);
	cout << "Welcom to Vitto's!" << endl;
	gotoxy(30, 10);
	cout << "Mangiare, Mangiare!" << endl;
	gotoxy(40, 23);
	cout << "Press any key to continue...";
	_getch();
}

//function that updates the vending machine based on user choice and returns the total
//purchases made back to main

unsigned short int update_vending(string items[], float prices[], unsigned short int quantity[])
{	
	//variable declariation and initializations 

	short unsigned int choice = 1;
	short unsigned int y_control = 6;
	char selection = ' ';
	unsigned short int selection_control;
	unsigned short int purchases = 0;

	// 1 ascii = 49 (serves as a reminder)
	//set up vending machine title and column headers 

	clrscr(0, 0);
	gotoxy(30, 2);
	cout << "VITTO'S VENDETTA VENDING" << endl;
	gotoxy(25, 4);
	cout << "CHOICE" << endl;
	gotoxy(35, 4);
	cout << "ITEM NAME" << endl;
	gotoxy(49, 4);
	cout << "PRICE" << endl;
	gotoxy(56, 4);
	cout << "QTY" << endl;

	//loop to output the vending machine menu

	for (short unsigned int i = 0; i < 8; i++)
	{
		if (i < 7)
		{
			gotoxy(27, y_control);
			cout << choice << "." << endl;
			gotoxy(32, y_control);
			cout << items[i] << endl;
			gotoxy(50, y_control);
			cout << prices[i] << endl;
			gotoxy(57, y_control);
			cout << quantity[i];
			choice++;
			y_control++;
		}

		else
		{
			gotoxy(27, y_control);
			cout << "E." << endl;
			gotoxy(32, y_control);
			cout << "No More Purchase" << endl;
			gotoxy(35, 15);
			cout << "Select an item:" << endl;
		}
	}

	//loop to keep going until the user has finished making all selection

	while (selection != 'e')
	{
		//prompt user for item selection 

		gotoxy(51, 15);
		selection = _getch();
		selection = tolower(selection);

		//determine parallel array element location and y-axis position

		selection_control = selection - 49;
		y_control = 6 + selection_control;

		//update item quantity based on user selection
		//check if the selected item is out

		switch (quantity[selection_control])
		{
			case 1:
				quantity[selection_control]--;
				gotoxy(57, y_control);
				cout << "OUT" << endl;
				purchases++;
				break;
			
			case 2:
			case 3:
			case 4:
			case 5:
				quantity[selection_control]--;
				gotoxy(57, y_control);
				cout << quantity[selection_control] << endl;
				purchases++;
				break;
	
			default:
				break;
		}
	}
	
	//return purchases back to main for use in the thanks_for_shopping function

	return purchases;
}

//functions that thanks user for their purchases and displays the ammount of purchases made

void thanks_for_shopping(unsigned short int purchases)
{	
	clrscr(0, 0);
	gotoxy(20, 8);
	cout << "You have purchased " << purchases << " number of items." << endl;
	gotoxy(20, 10);
	cout << "Thank You for Shopping at Vitto's!" << endl;
}

int main()
{
	//variable declartions and initalization for constant arrays and main loop control

	string items[] = { "Soprano Soup", "Godfather Pasta", "Gotti Gum", "Capone Crisps",
		"Gambino Pie", "Luciano Lunch", "Mafia Muffin" };
	float prices[] = { 4.75, 5.85, 2.50, 3.00, 6.75, 8.50, 1.75 };
	char again = 'y';

	while (again == 'y')
	{
		//variable declaration and initialization for variables that need to be refreshed up re-execution

		unsigned short int quantity[] = { 5, 5, 5, 5, 5, 5, 5 };

		//function calls 

		greetings();
		unsigned short int purchases = update_vending(items, prices, quantity);
		thanks_for_shopping(purchases);

		//check if the user would like to re-execute the program

		gotoxy(20, 20);
		cout << "Run the program again? (y/n):" << endl;
		gotoxy(50, 20);
		again = _getch();
		again = tolower(again);
	}

	//final prompt to close the program

	gotoxy(40, 22);
	cout << "Press any key to continue...";
	_getch();

	return 0;
}