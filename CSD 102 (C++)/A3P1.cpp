//Program Name: A3P1.cpp
//Programmer: Sebastian Malysa
//Purpose: Simulate a bookstore cashier system
//Date Written: 02/14/2016
//Date Last Modified: 02/18/2016

#include <iostream>
//#include <process.h>
//#include <iomanip>
#include <conio.h>
#include <string>
//#include <stdio.h>
//#include <ctype.h>
#include <windows.h> //required to implement screen coordinates
#include <iomanip>

using namespace std;

void gotoxy(int x, int y)
{
	COORD coord;
	coord.X = x;  //column coordinate
	coord.Y = y;	//row coordinate
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void clrscr(int x, int y)
{
	COORD                       coordScreen = { x, y };
	DWORD                       cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO  csbi;
	DWORD                       dwConSize;
	HANDLE                      hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
	FillConsoleOutputCharacter(hConsole, TEXT(' '),
		dwConSize, coordScreen, &cCharsWritten);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	FillConsoleOutputAttribute(hConsole, csbi.wAttributes,
		dwConSize, coordScreen, &cCharsWritten);
	SetConsoleCursorPosition(hConsole, coordScreen);
}

double isbn_purchase(short unsigned int isbn[], string book_title[], float retail[])
{
	short unsigned int y_control = 4;
	short unsigned int isbn_selection;
	bool isbn_check[] = { false, false, false, false, false }; 
	short unsigned int line_item = 0;
	char shopping = ' ';
	unsigned short int index;
	double pretax_total = 0.00;

	// output menu
	
	clrscr(0, 0);
	gotoxy(5, 2);
	cout << "ISBN" << endl;
	gotoxy(15, 2);
	cout << "Title" << endl;
	gotoxy(45, 2);
	cout << "Price" << endl;
	gotoxy(56, 2);
	cout << "Qty" << endl;
	gotoxy(65, 2);
	cout << "Total" << endl;
	gotoxy(5, 3);
	cout << "-----------------------------------------------------------------" << endl;

	// main loop 

	while (shopping != 't')
	{
		unsigned short int quantity = 0;
		double total = 0.00;

		// check for valid or repeat isbn

		for (int i = 0; i < 5; i++)
		{
			if (i == 0)
			{
				isbn_selection = 0; // initialized to 0 so that in the event more than 5 numbers 
									// were entered again the proper statements would execute 
				gotoxy(5, y_control);
				cin >> isbn_selection;
			}

			if (isbn_selection == isbn[i])
			{

				// check for repeat isbn

				if (isbn_check[i] == true)
				{
					y_control += 2;
					gotoxy(15, y_control);
					cout << "ISBN has already been selected..." << endl;
					gotoxy(49, y_control);
					_getch();
					cin.clear(); // used cin.clear here so it would ignore an error if more than 
								 // 5 numbers in the isbn input (this is used again below)
					y_control -= 2;
					clrscr(0, y_control);
					i = -1;
					continue;
				}

				isbn_check[i] = true;
				index = i;
				break;
			}

			// excuted if isbn is not valid 

			else if (i == 4)
			{
				y_control += 2;
				gotoxy(15, y_control);
				cout << "Invalid ISBN..." << endl;
				gotoxy(30, y_control);
				_getch();
				cin.clear();
				y_control -= 2;
				clrscr(0, y_control);
				i = -1;
				continue;
			}
		}

		gotoxy(15, y_control);
		cout << book_title[index] << endl;
		gotoxy(45, y_control);
		cout << setprecision(2) << fixed <<retail[index] << endl;

		// prompt for quantity 
		
		gotoxy(56, y_control);
		cin >> quantity; 

		// calulate total before tax 

		total = retail[index] * quantity;
		pretax_total += total;

		gotoxy(65, y_control);
		cout << "$" << setprecision(2) << fixed << total << endl;
		line_item += 1;

		// condition to break the loop if 5 line itmes have been entered

		if (line_item == 5)
		{
			break;
		}

		// prompt for 't' 

		gotoxy(73, y_control);
		shopping = _getch();
		shopping = tolower(shopping);
		
		y_control += 2;
	}

	return pretax_total;
}

void calculate_grand_total(double &total_taxes, double &grand_total, double &pretax_total)
{
	total_taxes = 0.00;
	grand_total = 0.00;
	float tax = 0.13;

	total_taxes = pretax_total * tax;
	grand_total = pretax_total + total_taxes;
}

void output_totals(double total_taxes, double grand_total, double pretax_total)
{	
	gotoxy(50, 16);
	cout << "Pre Tax Total" << endl;
	gotoxy(67, 16);
	cout << "$" << setprecision(2) << fixed << pretax_total << endl;
	gotoxy(50, 17);
	cout << "HST(13%)" << endl; 
	gotoxy(67, 17);
	cout << "$" << setprecision(2) << fixed << total_taxes << endl;
	gotoxy(50, 18);
	cout << "After Tax Total" << endl;
	gotoxy(67, 18);
	cout << "$" << setprecision(2) << fixed << grand_total << endl;
}

int main()
{	
	short unsigned int isbn[] = { 11110, 12221, 13332, 24443, 25554 };
	string book_title[] = { "Binary Kibbles and Bits", "Underwater Googles", "Vitamin C++",
		"Surf and Turf the Web", "Java-lin: Olympic Favourite" };
	float retail[] = { 155.95, 116.75, 126.50, 125.75, 138.99 };
	double total_taxes; 
	double grand_total;
	char again = 'y';

	while (again == 'y')
	{

		double pretax_total = isbn_purchase(isbn, book_title, retail);
		calculate_grand_total(total_taxes, grand_total, pretax_total);
		output_totals(total_taxes, grand_total, pretax_total);

		gotoxy(20, 20);
		cout << "Make another purchase? (y/n):" << endl;
		gotoxy(50, 20);
		again = _getch();
		again = tolower(again);
	}

	gotoxy(40, 22);
	cout << "Press any key to continue...";
	_getch();

	return 0;
}