//Program Name: A1P1.cpp
//Programmer: Sebastian Malysa
//Purpose: Yearly rainfall calculator that displays total yearly rainfall and
//		   the average, highest, lowest monthly rainfall amounts
//Date Written: 17/01/2016
//Date Last Modified: 24/01/2016

#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
	//Variable declarations (constants/variables that do not need to be refreshed)

	string months[] = { "January", "February", "March", "April", "May", "June", "July",
		"August", "September", "October", "November", "December" };
	char again = 'y';

	// Loop to check if user wants to execute the program again

	while (again == 'y')
	{
		//Variable declarations (variables that change/need to be refreshed upon reexecution)

		unsigned short int monthlyRain[12];
		unsigned short int totalRain = 0;
		float averageRain = 0;
		unsigned short int minRain = 0;
		unsigned short int maxRain = 0;
		string minMonths[12];
		string maxMonths[12];
		unsigned short int minControl = 0;
		unsigned short int maxControl = 0;

		//Gather monthly rainfall input from user

		for (unsigned short int i = 0; i < 12; i++)
		{
			cout << "\n\t\tPlease enter the rainfall for the month of "
				<< months[i] << ": ";
			cin >> monthlyRain[i];

			//Find min and max monthly rainfall

			if (i == 0)
			{
				minRain = monthlyRain[i];
				maxRain = monthlyRain[i];
			}

			if (monthlyRain[i] < minRain)
				minRain = monthlyRain[i];
			if (monthlyRain[i] > maxRain)
				maxRain = monthlyRain[i];

			//Calculate the total rainfall

			totalRain += monthlyRain[i];
		}

		//Calculate average rainfall

		averageRain = totalRain / 12.0;

		//Determine months with the min and max rainfall 

		for (unsigned short int i = 0; i < 12; i++)
		{

			if (monthlyRain[i] == maxRain)
			{
				maxMonths[maxControl] = months[i];
				maxControl++;
			}

			if (monthlyRain[i] == minRain)
			{
				minMonths[minControl] = months[i];
				minControl++;
			}

		}

		//Output total and average rainfall

		cout << "\n\n\t\tTotal Rainfall: " << totalRain << " mm" << endl;
		cout << "\t\tAverage Rainfall: " << setprecision(1) << fixed << averageRain << " mm" << endl;

		//Check if all months have the same rainfall if not output month(s) with highest and lowest rainfall

		if (minRain == maxRain)
		{
			cout << "\n\t\tAll months had the same rainfall this year.";
		}

		else
		{
			cout << "\n\t\tMonth(s) with highest rainfall are: ";
			for (unsigned short int i = 0; i < maxControl; i++)
			{
				cout << maxMonths[i];
				if (i < maxControl - 1)
				{
					cout << ", ";
				}
			}

			cout << "\n\t\tMonth(s) with lowest rainfall are: ";
			for (unsigned short int i = 0; i < minControl; i++)
			{
				cout << minMonths[i];
				if (i < minControl - 1)
				{
					cout << ", ";
				}
			}
		}

		cout << "\n\n\t\tWould you like to run the program again? (y/n): ";
		cin >> again;
	}
	cout << endl;

	cin.ignore();
	cout << "\n\n\n\n\t\tPress any key to continue";
	cin.get();

	return 0;
}