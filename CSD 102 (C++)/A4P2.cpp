//Program Name: A4P2.cpp
//Programmer: Sebastian Malysa
//Purpose: The Hangman Game
//Date Written: 01/03/2016
//Date Last Modified: 06/03/2016

#include <iostream>
//#include <process.h>
//#include <iomanip>
//#include <stdio.h>
//#include <ctype.h>
#include <conio.h>
#include <string>
#include <windows.h> //required to implement screen coordinates
#include <iomanip>
#include <time.h>

using namespace std;

void gotoxy(int x, int y)
{
	COORD coord;
	coord.X = x;  //column coordinate
	coord.Y = y;	//row coordinate
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void clrscr(int x, int y)
{
	COORD                       coordScreen = { x, y };
	DWORD                       cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO  csbi;
	DWORD                       dwConSize;
	HANDLE                      hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
	FillConsoleOutputCharacter(hConsole, TEXT(' '),
		dwConSize, coordScreen, &cCharsWritten);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	FillConsoleOutputAttribute(hConsole, csbi.wAttributes,
		dwConSize, coordScreen, &cCharsWritten);
	SetConsoleCursorPosition(hConsole, coordScreen);
}

string StartNewGame(bool &Play)
{
	string GameWords[10] = { "URN", "MEGAHERTZ", "PNEUMONIA", "RHUBARB", "POLKA",
							 "QUOTIDIAN", "KHAKI", "IVY", "VAPORIZE", "CONUNDRUM" };
	string GameWord;
	char Choice = '1';

	gotoxy(28, 2);
	cout << char(0x01) << " Welcome to Hangman " << char(0x01);
	gotoxy(5, 3);
	cout << "---------------------------------------------------------------------";

	clrscr(1, 4);
	gotoxy(30, 8);
	cout << "1. New Game";
	gotoxy(30, 9);
	cout << "E. Exit";
	
	gotoxy(30, 12);
	cout << "Submit Your Choice: ";

	while (Choice)
	{	
		clrscr(50, 12);
		gotoxy(50, 12);
		cin >> Choice;
		Choice = toupper(Choice);

		if (Choice == '1')
		{
			GameWord = GameWords[rand() % 10];
			break;
		}
		
		else if (Choice == 'E')
		{
			Play = false;
			break;
		}
		
		// In the event something other than 1 or E is selected

		else
		{
			gotoxy(50, 12);
			clrscr(50, 12);
			cout << "Oops!";
			Sleep(0750);
			cin.clear();
			cin.ignore(10000, '\n');
			continue;
		}
	}

	return GameWord;

}

void DrawHangman(unsigned short int &BadLetter)
{
	BadLetter++;

	switch (BadLetter)
	{
		case 1: 
			gotoxy(58, 9);
			cout << char(0x01);
			break;
		case 2:
			gotoxy(58, 10);
			cout << char(0xDB);
			break;
		case 3:
			gotoxy(57, 10);
			cout << char(0x2F);
			break;
		case 4:
			gotoxy(59, 10);
			cout << char(0x5C);
			break;
		case 5:
			gotoxy(57, 11);
			cout << char(0x2F);
			break;
		case 6:
			gotoxy(59, 11);
			cout << char(0x5C);
	}
}

void LetterMatchInWord(string Alphabet, string GameWord, char Letter, unsigned short int &GoodLetter, 
					   unsigned short int &BadLetter, bool Selected[])
{	

	short unsigned int Hangman = GoodLetter;

	// Find the letter in the Alphabet and remove it

	for (unsigned short int i = 0; i < Alphabet.length(); i++)
	{
		// In the event something other than a letter is used

		if ((Letter < 65) || (Letter > 90))
		{
			DrawHangman(BadLetter);
			gotoxy(33, 19);
			cout << "Letters Only Please";
			break;
		}
		
		else if (i == Alphabet.find(Letter, i))
		{

			// Check if the letter was already selected

			if (Selected[i] == true)
			{
				DrawHangman(BadLetter);
				gotoxy(34, 19);
				cout << "Already Selected";
				break;
			}
			
			else
			{
				gotoxy((i * 2 + 14), 5);
				cout << " ";
				Selected[i] = true;

				// Find player choice in the game word and appropriately output to the screen

				for (unsigned short int x = 0; x < GameWord.length(); x++)
				{
					if (x == GameWord.find(Letter, x))
					{
						gotoxy((x * 2 + 22), 11);
						cout << Letter;
						GoodLetter++;
					}
				}

				// If the letter isn't in the word output the appropriate hangman body part

				if (Hangman == GoodLetter)
				{
					DrawHangman(BadLetter);
				}
			}
		}
	}
}


void GetLetterFromUser(string GameWord)
{
	string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char Letter;
	unsigned short int GoodLetter = 0;
	unsigned short int BadLetter = 0;
	bool Selected[26];

	clrscr(1, 4);

	// Output for the Alphabet 

	for (unsigned short int x = 0; x < Alphabet.length(); x++)
	{
		gotoxy((x * 2 + 14), 5);
		cout << Alphabet[x];
	}

	// Output for the gallows 

	for (unsigned short int y = 0; y < 10; y++)
	{
		if (y == 0)
		{
			gotoxy(51, 7);
			cout << char(0xC9) << char(0xCD) << char(0xCD) << char(0xCD) << char(0xCD)
				 << char(0xCD) << char(0xCD) << char(0xB8);
			gotoxy(58, 8);
			cout << char(0x7C);
		}
		
		else if (y == 9)
		{
			gotoxy(50, 15);
			cout << char(0xDF) << char(0xDF) << char(0xDF);
		}

		else
		{
			gotoxy(51, (y + 7));
			cout << char(0xBA);
		}
	}

	// Hidden game word output 

	for (unsigned short int z = 0; z < GameWord.length(); z++)
	{
		gotoxy((z * 2 + 22), 11);
		cout << "_";
	}
	
	// Letter selection prompt 

	gotoxy(30, 17);
	cout << "Choose a letter: ";
	
	while (GoodLetter < GameWord.length())
	{
		gotoxy(47, 17);
		Letter = _getch();
		Letter = toupper(Letter);
		clrscr(47, 17);

		LetterMatchInWord(Alphabet, GameWord, Letter, GoodLetter, BadLetter, Selected);

		// Lose condition

		if (BadLetter == 6)
		{
			clrscr(47, 17);
			gotoxy(47, 17);
			cout << "You Lose!";
			gotoxy(47, 18);
			cout << "The word was " << GameWord;
			break;
		}

		// Win condition

		else if (GoodLetter == GameWord.length())
		{
			clrscr(47, 17);
			gotoxy(47, 17);
			cout << "Congratulations! You Win!";
		}
	}

	gotoxy(5, 20);
	cout << "---------------------------------------------------------------------";
	gotoxy(17, 22);
	cout << "Press any key to return to the main menu...";
	_getch();
}

int main()
{
	srand(time(NULL));

	bool Play = true; 

	while (Play == true)
	{
		string GameWord = StartNewGame(Play);

		if (Play == false)
		{
			break;
		}

		GetLetterFromUser(GameWord);
	}

	return 0;
}
