//Program Name: A4P1.cpp
//Programmer: Sebastian Malysa
//Purpose: Randomly generate a phrase
//Date Written: 02/25/2016
//Date Last Modified: 02/26/2016

#include <iostream>
#include <conio.h>
#include <string>
#include <cstring>
#include <windows.h> //required to implement screen coordinates
#include <iomanip>
#include <time.h>

using namespace std;

void gotoxy(int x, int y)
{
	COORD coord;
	coord.X = x;  //column coordinate
	coord.Y = y;	//row coordinate
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void clrscr(int x, int y)
{
	COORD                       coordScreen = { x, y };
	DWORD                       cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO  csbi;
	DWORD                       dwConSize;
	HANDLE                      hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
	FillConsoleOutputCharacter(hConsole, TEXT(' '),
		dwConSize, coordScreen, &cCharsWritten);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	FillConsoleOutputAttribute(hConsole, csbi.wAttributes,
		dwConSize, coordScreen, &cCharsWritten);
	SetConsoleCursorPosition(hConsole, coordScreen);
}

void print_phrase(char phrase[])
{
	phrase[0] = toupper(phrase[0]);
	cout << "\n\t\t" << phrase << endl;
}

void create_phrase(string noun[], string verb_adj[])
{
	char phrase[60];
	unsigned short int rand_1;
	unsigned short int rand_2;
	unsigned short int rand_3;
	string rand_noun_1;
	string rand_verb_adj;
	string rand_noun_2;

	for (unsigned short int i = 0; i < 5; i++)
	{
		rand_1 = rand() % 6;
		rand_2 = rand() % 3;
		rand_3 = rand() % 6;

		while (rand_1 == rand_3)
		{
			rand_3 = rand() % 6;
		}
		
		rand_noun_1 = noun[rand_1];
		rand_verb_adj = verb_adj[rand_2];
		rand_noun_2 = noun[rand_3];

		// not sure why, but the compiler made me use _s with the strcpy and strcat functions
		// or else it would not work 

		// creates the full phrase 
		strcpy_s(phrase, rand_noun_1.c_str());
		strcat_s(phrase, " ");
		strcat_s(phrase, rand_verb_adj.c_str());
		strcat_s(phrase, " ");
		strcat_s(phrase, rand_noun_2.c_str());
		strcat_s(phrase, ".");

		print_phrase(phrase);
	}
}

int main()
{
	srand(time(NULL));

	string noun[6] = { "programmers", "snowboarders", "drummers", "networkers",
		"bass players", "engineers" };
	string verb_adj[3] = { "are tougher than", "are not as cool as", "are more popular than" };
	char again = 'y';

	while (again == 'y')
	{
		clrscr(0, 0);
		
		cout << "\n\t\t\t\tPhrase Generator" << endl;
		cout << "\t\t_______________________________________________" << endl;

		create_phrase(noun, verb_adj);

		cout << "\t\t_______________________________________________" << endl;

		cout << "\n\t\tGenerate another phrase? (y/n): ";
		again = _getch();
		again = tolower(again);
	}

	cout << "\n\n\t\t\tPress any key to continue...";
	_getch();

	return 0;
}