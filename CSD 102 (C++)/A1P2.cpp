//Program Name: A1P2.cpp
//Programmer: Sebastian Malysa
//Purpose: Calculates the percentage of revenue a movie theater earns from ticket sales
//Date Written: 17/01/2016
//Date Last Modified: 24/01/2016

#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
	// Loop to check if user wants to execute the program again

	char again = 'y';
	while (again == 'y')
	{

		//Variable declarations 

		string movie = "";
		unsigned short int aTickets = 0;
		unsigned short int cTickets = 0;
		float aPrice = 22.00;
		float cPrice = 15.00;
		float gross = 0.00;
		float net = 0.00;
		float profitPercent = 0.45;
		float distributorFee = 0.00;

		//Gather user input

		cout << "\n\n\t\tMovie name: ";
		cin.ignore();
		getline(cin, movie);

		cout << "\n\t\tNumber of adult tickets: ";
		cin >> aTickets;

		cout << "\n\t\tNumber of child tickets: ";
		cin >> cTickets;

		//Calculate the gross box office profit

		gross = (aTickets * aPrice) + (cTickets * cPrice);

		//Calculate the net box office profit 

		net = gross * profitPercent;

		//Calculate amount paid to the distributor 

		distributorFee = gross - net;

		//Output results 
		//cout << setw(5)<<left<<"Movie Name: " .... 

		cout << setprecision(2) << fixed;
		cout << endl;
		
		cout << setw(16) << " " << setw(30) << left << "Movie Name: " 
			<< setw(5) << " " << setw(25) << left << movie << endl;
		
		cout << setw(16) << " " << setw(30) << left << "Adult Tickets Sold: " 
			<< setw(5) << " " << setw(10) << right << aTickets << endl;
		
		cout << setw(16) << " " << setw(30) << left << "Child Tickets Sold: " 
			<< setw(5) << " " << setw(10) << right << cTickets << endl;
		
		cout << setw(16) << " " << setw(30) << left << "Gross Box Office Profit: " 
			<< setw(5) << left << "$ " << setw(10) << right << gross << endl;
		
		cout << setw(16) << " " << setw(30) << left << "Net Box Office Profit: " 
			<< setw(5) << left << "$ " << setw(10) << right << net << endl;
		
		cout << setw(16) << " " << setw(30) << left << "Amount Paid to Distributor: " 
			<< setw(5) << left << "$ " << setw(10) << right << distributorFee << endl;

		cout << "\n\n\t\tWould you like to run the program again? (y/n): ";
		cin >> again;

	}

	cin.ignore();
	cout << "\n\n\n\n\t\tPress any key to continue ";
	cin.get();

	return 0;
}