//Program Name: A4P1.cpp
//Programmer: Sebastian Malysa
//Purpose: Randomly generate a phrase
//Date Written: 02/25/2016
//Date Last Modified: 02/26/2016

#include <iostream>
#include <conio.h>
#include <string>
#include <windows.h> //required to implement screen coordinates
#include <iomanip>
#include <time.h>

using namespace std;

void gotoxy(int x, int y)
{
	COORD coord;
	coord.X = x;  //column coordinate
	coord.Y = y;	//row coordinate
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void clrscr(int x, int y)
{
	COORD                       coordScreen = { x, y };
	DWORD                       cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO  csbi;
	DWORD                       dwConSize;
	HANDLE                      hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
	FillConsoleOutputCharacter(hConsole, TEXT(' '),
		dwConSize, coordScreen, &cCharsWritten);
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	FillConsoleOutputAttribute(hConsole, csbi.wAttributes,
		dwConSize, coordScreen, &cCharsWritten);
	SetConsoleCursorPosition(hConsole, coordScreen);
}

void print_phrase(string phrase[])
{
	clrscr(0, 0);

	cout << "\n\t\t\t\tPhrase Generator" << endl;
	cout << "\t\t_______________________________________________";

	for (unsigned short int i = 0; i < 5; i++)
	{
		cout << "\n\n\t\t" << phrase[i] << endl;
	}

	cout << "\t\t_______________________________________________" << endl;
}

void create_phrase(string noun[], string verb_adj[])
{
	string phrase[5];
	string first_noun;

	for (unsigned short int i = 0; i < 5; i++)
	{
		unsigned short int rand_noun1 = rand() % 6;
		unsigned short int rand_noun2 = rand() % 6;
		unsigned short int rand_verb_adj = rand() % 3;

		while (rand_noun1 == rand_noun2)
		{
			rand_noun2 = rand() % 6;
		}

		first_noun = noun[rand_noun1];
		first_noun[0] = toupper(first_noun[0]);

		phrase[i] = (first_noun + " " + verb_adj[rand_verb_adj] + " " + noun[rand_noun2] + ".");
	}

	print_phrase(phrase);
}

int main()
{
	srand(time(NULL));

	string noun[6] = { "programmers", "snowboarders", "drummers", "networkers",
		"bass players", "engineers" };
	string verb_adj[3] = { "are tougher than", "are not as cool as", "are more popular than" };
	char again = 'y';

	while (again == 'y')
	{
		create_phrase(noun, verb_adj);

		cout << "\n\t\tGenerate another phrase? (y/n): ";
		again = _getch();
		again = tolower(again); 
	}

	cout << "\n\n\t\t\tPress any key to continue...";
	_getch();

	return 0;
}