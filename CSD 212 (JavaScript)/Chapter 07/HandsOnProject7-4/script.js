/*	JavaScript 6th Edition
 *	Chapter 7
 *	Hands-on Project 7-4
 
 *	Outer Orbits
 *	Author: Sebastian Malysa
 *	Date:	4 / 4 / 2016
 
 *	Filename: script.js
 */
 
 "use strict"; //interpret contents in JavaScript strict mode

 var delivInfo = {};
 var delivSummary = document.getElementById("deliverTo")

 function processDeliveryInfo()
 {
 	var prop;
 	delivInfo.name = document.getElementById("nameinput").value;
 	delivInfo.addr = document.getElementById("addrinput").value;
 	delivInfo.city = document.getElementById("cityinput").value;
 	delivInfo.email = document.getElementById("emailinput").value;
 	delivInfo.phone = document.getElementById("phoneinput").value;

 	for (prop in delivInfo)
 	{
 		delivSummary.innerHTML += "<p>" + delivInfo[prop] + "</p>";
 	}
 }

 function previewOrder()
 {
 	processDeliveryInfo();
	document.getElementsByTagName("section")[0].style.display = "block";
	document.getElementById("deliverTo").style.display = "block";
	document.getElementsByTagName("h3")[1].style.display = "none";
 }

 function createEventListeners()
 {
 	var previewButton = document.getElementById("previewBtn");

 	if (previewButton.addEventListener)
 	{
 		previewButton.addEventListener("click", previewOrder, false);
 	}	

 	else if (previewButton.attachEvent)
 	{
 		previewButton.attachEvent("onclick", previewOrder);
 	}
 }

if (window.addEventListener)
{
	window.addEventListener("load", createEventListeners, false);
}

else if (window.attachEvent)
{
	window.attachEvent("onload", createEventListeners);
}