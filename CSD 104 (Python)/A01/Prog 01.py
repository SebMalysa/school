# Program Name:       Prog01.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 28/09/2015
# Purpose of Program: To calculate tax and total of five purchased items

price1 = float(input('\n\n\t\tPrice of item 1: $'))
price2 = float(input('\n\n\t\tPrice of item 2: $'))
price3 = float(input('\n\n\t\tPrice of item 3: $'))
price4 = float(input('\n\n\t\tPrice of item 4: $'))
price5 = float(input('\n\n\t\tPrice of item 5: $'))

subtotal = (price1 + price2 + price3 + price4 + price5)
sales_tax = (subtotal * .06)
total = (subtotal + sales_tax)

print('\n\t\t\tSubtotal: $', format(subtotal, '.2f'))
print('\n\t\t\tSales Tax: $', format(sales_tax, '.2f'))
print('\n\t\t\tTotal: $', format(total, '.2f'))

