# Program Name:       A1P1.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 09/29/2015
# Purpose of Program: To convert temperature from Celsius to Fahrenheit

Celsius = float(input('\n\n\t\tTemperature in Celsius: '))

Fahrenheit = (9 / 5 * Celsius + 32)

print('\n\t\t\tTemperature in Fahrenheit: ', format(Fahrenheit, '.1f'))

