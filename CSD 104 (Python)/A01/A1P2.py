# Program Name:       A1P1.py
# Programmer Name:    Sebastian Malysa
# Date Last Compiled: 09/29/2015
# Purpose of Program: To calculate how much money Joe made during the purchase and selling
#                     of shares in a stock after paying a commission to his stock broker.

Shares_Purchased = 2000
Purchase_Price = 40.00
Broker_Commission = 0.03
Shares_Sold = 2000
Selling_Price = 42.75

Money_Paid = (Shares_Purchased * Purchase_Price)
Purchase_Commission = (Money_Paid * Broker_Commission)
Money_Earned = (Shares_Sold * Selling_Price)
Selling_Commission = (Money_Earned * Broker_Commission)
Money_Left = (Money_Earned - Purchase_Commission - Selling_Commission)

print('\n\t\t\tMoney Paid: $', format(Money_Paid, '.2f'))
print('\n\t\t\tPurchase Commission: $', format(Purchase_Commission, '.2f'))
print('\n\t\t\tMoney Earned: $', format(Money_Earned, '.2f'))
print('\n\t\t\tSelling Commission: $', format(Selling_Commission, '.2f'))
print('\n\t\t\tMoney Left: $', format(Money_Left, '.2f'))
