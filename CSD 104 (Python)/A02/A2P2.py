# Program Name:       A2P2.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 10/08/2015
# Purpose of Program: Calculate the total based on packages purchased

packages_purchased = int(input('\n\n\t\tNumber of Packages Purchased: '))
package_value = 99.00
discount = 0.00

if packages_purchased >= 100:
    discount = packages_purchased * package_value * 0.4
elif packages_purchased >= 50:
    discount = packages_purchased * package_value * 0.3
elif packages_purchased >= 20:
    discount = packages_purchased * package_value * 0.2
elif packages_purchased >= 10:
    discount = packages_purchased * package_value * 0.1
elif packages_purchased < 10:
    discount = 0.00

total = packages_purchased * package_value - discount

print('\n\t\t\tDiscount: $', format(discount, '.2f'))
print('\n\t\t\tTotal: $', format(total, ".2f"))
