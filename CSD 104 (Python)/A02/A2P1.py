# Program Name:       A2P1.py
# Programmer Name:    Sebastian Malysa
# Date Last Compiled: 10/08/2015
# Purpose of Program: Users input a combination of coins to make up exactly one dollar

Pennies = int(input('\n\n\t\tNumber of Pennies: '))
Nickels = int(input('\n\n\t\tNumber of Nickels: '))
Dimes = int(input('\n\n\t\tNumber of Dimes: '))
Quarters = int(input('\n\n\t\tNumber of Quarters = '))

Total = (Pennies * 0.01) + (Nickels * 0.05) + (Dimes * 0.1) + (Quarters * 0.25)

if Total == 1.00:
    print('\n\t\t\tCongratulations, You Win!')

elif Total > 1.00:
    print('\n\t\t\tThe Amount Entered Was More Than One Dollar, You Lose!')

elif Total < 1.00:
    print('\n\t\t\tThe Amount Entered Was Less Than One Dollar, You Lose')
