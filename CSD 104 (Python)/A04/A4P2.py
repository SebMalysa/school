# Program Name:       A4P2.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 11/02/2015
# Purpose of Program: Calculates amount of money earned over a period of time
#                     if a salary is one penny the first day and doubles each day after

import os

days_worked = int(input('\n\n\tEnter days worked in pay period: '))
salary = 0.01
day = 0
total = 0

print('\n\t\t\tDay\t\tSalary')
print('\t\t\t---------------')

for day in range(days_worked):
    total += salary
    day += 1
    print('\t\t  %4.i      $%s' % (day, format(salary, ',.2f')))
    salary *= 2

if day > 1:
    print('\n\n\tYou have earned $%s in %i days.' % (format(total, ',.2f'), day))

else:
    print('\n\n\tYou have earned $0.01 in 1 day.')

print('\n')

os.system('pause')
