# Program Name:       A4P1.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 11/02/2015
# Purpose of Program: To display a table of the Celsius temperatures from
#                     0-20 and their Fahrenheit equivalents

import os

print('\n\t Celsius\tFahrenheit')
print('\t-----------------------')

for Celsius in range(21):
    Fahrenheit = (9 / 5 * Celsius + 32)
    print('\t\t%2.i\t\t   %4.1f' % (Celsius, Fahrenheit))

print('\n')

os.system("pause")
