# Program Name:       A6P1.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 11/20/2015
# Purpose of Program: Calculates salesperson commission

import os

name = ''
sales = 0.0
years_emp = 0
com = 0.0
com_rate1 = 0.05
com_rate2 = 0.1
com_rate3 = 0.2
bonus = 10


def input_salesperson_data():

    global name, sales, years_emp

    name = input("\n\n\n\tEnter salesperson's name: ")
    sales = float(input("\n\tEnter dollar amount of sales: "))
    years_emp = int(input("\n\tEnter years employed: "))


def calculate_commission():

    global com

    if sales <= 1000:
        com = sales * com_rate1

    elif sales <= 2000:
        com = sales * com_rate2

    else:
        com = sales * com_rate3


def determine_bonus():

    global com

    if years_emp > bonus:
        com *= 2


def output_commission():

    print("\n\n\t\t\t\tCommission Report")
    print("\n\tSalesperson Name\t\t\tCommission Amount")
    print("\n\t{:s}\t\t\t\t\t\t\t${:,.2f}" .format(name, com))


def main():

    again = 'y'

    while again.lower() == 'y':

        input_salesperson_data()
        calculate_commission()
        determine_bonus()
        output_commission()

        again = input('\n\n\t\tWould you like to process another employee? (y/n): ')

        while again not in ('y', 'Y', 'n', 'N'):
            print('\n\t\t\tPlease enter y or n.\n')
            again = input('\n\t\t\t Would you like to process another employee? (y/n): ')

        if again.lower() == 'n':
            print('\n\t\t\tGoodbye.')

main()

print('\n\n')
os.system("pause")
