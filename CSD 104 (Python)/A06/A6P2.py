# Program Name:       A6P2.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 11/20/2015
# Purpose of Program: Calculates average grade for individual students
#                     and the entire class

import os

name = ''
test_1 = 0.0
test_2 = 0.0
test_3 = 0.0
average_num = 0.0
average_let = ''
student_total = 0
test_total = 0.0
class_ave = 0.0


def input_student_data():

    global name, test_1, test_2, test_3

    name = input('\n\n\n\tEnter student name: ')
    test_1 = float(input('\n\tEnter the result of Test 1: '))
    test_2 = float(input('\n\tEnter the result of Test 2: '))
    test_3 = float(input('\n\tEnter the result of Test 3: '))


def calculate_average():

    global average_num

    average_num = (test_1 + test_2 + test_3) / 3


def determine_letter_grade():

    global average_let

    if average_num >= 80:
        average_let = 'A'

    elif average_num >= 70:
        average_let = 'B'

    elif average_num >= 60:
        average_let = 'C'

    elif average_num >= 50:
        average_let = 'D'

    else:
        average_let = 'F'


def calculate_totals():

    global student_total, test_total, class_ave

    student_total += 1
    test_total += average_num
    class_ave = test_total / student_total


def output_student_data():

    print('\n\n\t\t\t\t\t\tStudent Grade Report')
    print('\n\tStudent Name\t\t\tNumeric Average\t\t\tLetter Grade')
    print('\n\t{:s}' .format(name) .ljust(26) + '{:.1f}%' .format(average_num)
          .ljust(25) + '{:s}' .format(average_let))


def output_class_totals():

    print('\n\n\tTotal students processed: {:d}' .format(student_total))
    print('\n\tClass average: {:.1f}%' .format(class_ave))


def main():

    again = 'y'

    while again.lower() == 'y':

        input_student_data()
        calculate_average()
        determine_letter_grade()
        calculate_totals()
        output_student_data()

        again = input('\n\n\t\tWould you like to process another student? (y/n): ')

        while again not in ('y', 'Y', 'n', 'N'):
            print('\n\t\t\tPlease enter y or n.\n')
            again = input('\n\t\t\tWould you like to process another student? (y/n): ')

        if again.lower() == 'n':
            output_class_totals()
            exit()

main()

os.system('pause')
