# Program Name:       A3P1.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 10/22/2015
# Purpose of Program: Converts kilometers to miles and vice versa

import os

print("\n\t\t\tDistance Conversion System")
print("\n\n\t\t1.\t Convert: Kilometers into Miles")
print("\t\t2.\t Convert: Miles into Kilometers")
print("\t\t3.\t Exit Program")

option = int(input("\n\t\t\t Option: "))
km = 0
m = 0

if option == 1:
    km = float(input("\n\n\t\tDistance in kilometers: "))
    m = km * 0.6213711
    print("\n\t\t\t", format(km, ",.1f"), "kilometers are equivalent to",
          format(m, ",.1f"), "miles.\n")

elif option == 2:
    m = float(input("\n\n\t\tDistance in miles:"))
    km = m * 1.609344
    print("\n\t\t\t", format(m, ",.1f"), "miles are equivalent to",
          format(km, ",.1f"), "kilometers.\n")

elif option == 3:
    print("\n\t\tGoodbye!\n")

else:
    print("\n\t\tIncorrect choice, please run program again...\n")

os.system("pause")
