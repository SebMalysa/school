# Program Name:       A3P1.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 10/22/2015
# Purpose of Program: Generates a random number between 1 - 10 and
#                     gives the user three chances to guess the number

import os
import random

print("\n\t\t\t\tNumber Guessing Game")

gus_num = int(input('\n\n\t\tGuess the number between 1 and 10: '))
ran_num = random.randint(1, 10)
guesses = 1

if gus_num > ran_num:
    print("\n\t\t\tSorry,too high.\n\t\t\tTry again.")
    gus_num = int(input('\n\n\t\tGuess the number between 1 and 10: '))
    guesses += 1

elif gus_num < ran_num:
    print("\n\t\t\tSorry, too low.\n\t\t\tTry again.")
    gus_num = int(input('\n\n\t\tGuess the number between 1 and 10: '))
    guesses += 1

if gus_num > ran_num:
    print("\n\t\t\tSorry,too high.\n\t\t\tTry again.")
    gus_num = int(input('\n\n\t\tGuess the number between 1 and 10: '))
    guesses += 1

elif gus_num < ran_num:
    print("\n\t\t\tSorry, too low.\n\t\t\tTry again.")
    gus_num = int(input('\n\n\t\tGuess the number between 1 and 10: '))
    guesses += 1

if gus_num != ran_num:
    print("\n\t\t\tSorry, you have no more guesses.")
    print("\t\t\tThe number is ", ran_num, ".", sep="")
    print("\n")

if gus_num == ran_num:
    print("\n\t\t\tCongratulations, you got it!")

    if guesses == 1:
        print("\t\t\tYou guessed the number in", guesses, "try.\n")

    else:
        print("\t\t\tYou guessed the number in", guesses, "tries.\n")

os.system("pause")
