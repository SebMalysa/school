# Program Name:       A8P3.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 12/07/2015
# Purpose of Program: Yearly rainfall calculator that displays total yearly rainfall and
#                     the average, highest, lowest monthly rainfall amounts


import os


def input_monthly_rainfall():

    months = ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
              'September', 'October', 'November', 'December')
    monthly_rainfall = []
    month = 0

    while month < len(months):

        try:

            rainfall = float(input('\n\n\t\tEnter total rainfall in mm for the month of'
                                   ' {:}: ' .format(months[month])))
            if rainfall < 0:
                print('\n\t\t\tTotal rainfall cannot be less than 0 mm')
                continue

            monthly_rainfall.append(rainfall)

            month += 1

        except ValueError:
            print('\n\t\t\tPlease enter a total monthly rainfall amount in mm.')
            continue

    return months, monthly_rainfall


def calculate_total_rainfall(monthly_rainfall):

    total = 0

    for rainfall in monthly_rainfall:
        total += rainfall

    return total


def calculate_average_rainfall(total, monthly_rainfall):

    average_rainfall = total / len(monthly_rainfall)

    return average_rainfall


def find_max_rainfall(monthly_rainfall, months):

    max_rain = 0
    max_monthly_rainfall = []

    max_rainfall = max(monthly_rainfall)

    for rainfall in monthly_rainfall:
        if rainfall == max_rainfall:
            max_monthly_rainfall.append(months[max_rain])

        max_rain += 1

    return max_rainfall, max_monthly_rainfall


def find_min_rainfall(monthly_rainfall, months):

    min_rain = 0
    min_monthly_rainfall = []

    min_rainfall = min(monthly_rainfall)

    for rainfall in monthly_rainfall:
        if rainfall == min_rainfall:
            min_monthly_rainfall.append(months[min_rain])

        min_rain += 1

    return min_rainfall, min_monthly_rainfall


def output_totals(total, average_rainfall, max_monthly_rainfall, max_rainfall,
                  min_monthly_rainfall, min_rainfall):

    print('\n\n\t\tThe total rainfall for the year was {:.1f} mm.' .format(total))
    print('\n\t\tThe average rainfall for the year was {:.1f} mm.' .format(average_rainfall))

    if max_rainfall == min_rainfall:
        print('\n\t\tAll 12 months had the same rainfall ({:.1f} mm) this year'
              .format(max_rainfall))

    else:
        print('\n\t\t' + ', ' .join(max_monthly_rainfall) + ' had the most rainfall '
              '({:.1f} mm) this year.' .format(max_rainfall))
        print('\n\t\t' + ', ' .join(min_monthly_rainfall) + ' had the least rainfall '
              '({:.1f} mm) this year.' .format(min_rainfall))


def main():

    again = 'y'

    while again.lower() == 'y':

        months, monthly_rainfall = input_monthly_rainfall()
        total = calculate_total_rainfall(monthly_rainfall)
        average_rainfall = calculate_average_rainfall(total, monthly_rainfall)
        max_rainfall, max_monthly_rainfall = find_max_rainfall(monthly_rainfall, months)
        min_rainfall, min_monthly_rainfall = find_min_rainfall(monthly_rainfall, months)
        output_totals(total, average_rainfall, max_monthly_rainfall, max_rainfall,
                      min_monthly_rainfall, min_rainfall)

        again = input('\n\n\t\tWould you like to calculate the rainfall for another year? (y/n): ')

        while again not in ('y', 'Y', 'n', 'N'):
            print('\n\t\t\tPlease enter y or n.\n')
            again = input('\n\t\tWould you like to calculate the rainfall for another year? (y/n): ')

        if again.lower() == 'n':
            print('\n\t\t\tGoodbye.')


main()

print('\n\n')
os.system("pause")
