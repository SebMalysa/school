# Program Name:       A8P1.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 12/07/2015
# Purpose of Program: Calculates a store's total weekly sales and
#                     displays the results

import os


def input_weekly_sales():

    week = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday')
    sales = []
    day = 0

    while day < len(week):

        try:

            sale = float(input('\n\n\t\tEnter total dollar amount of sales for {:}: ' .format(week[day])))

            if sale < 0:
                print('\n\t\t\tDollar amount cannot be less than 0')
                continue

            sales.append(sale)

            day += 1

        except ValueError:
            print('\n\t\t\tPlease enter a dollar amount.')
            continue

    return sales


def calculate_total_sales(sales):

    total = 0.0

    for dollars in sales:
        total += dollars

    return total


def output_total(total):

    print('\n\n\t\t\tThe total dollar amount of sales for the week is ${:,.2f}.' .format(total))


def main():

    again = 'y'

    while again.lower() == 'y':

        sales = input_weekly_sales()
        total = calculate_total_sales(sales)
        output_total(total)

        again = input('\n\t\t\tWould you like to calculate total sales for another week? (y/n): ')

        while again not in ('y', 'Y', 'n', 'N'):
            print('\n\t\t\tPlease enter y or n.\n')
            again = input('\n\t\t\tWould you like to calculate total sales for '
                          'another week? (y/n): ')

        if again.lower() == 'n':
            print('\n\t\t\tGoodbye.')


main()

print('\n\n')
os.system("pause")
