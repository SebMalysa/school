# Practice with objects

import time


class Car(object):

    def __init__(self, colour, year):
        self.colour = colour
        self.year = year

    def describe(self):
        print(self.colour, self.year)

micra = Car('Executive Grey', 2015)
f150 = Car('Midnight Abyss', 2014)

micra.describe()
f150.describe()


class Box(object):

    def __init__(self, length, width, height):
        self.length = length
        self.width = width
        self.height = height
        self._volume = None

    @property
    def volume(self):
        if self._volume is None:
            self._volume = self.length * self.width * self.height
            time.sleep(1)
        return self._volume

box_1 = Box(102, 150, 250)
box_2 = Box(5, 5, 10)

print(box_1.volume)
print(box_2.volume)
print(box_1.volume)
print(box_2.volume)

char_counts = {}
counts = {}

sentence = 'what it is'

for s in sentence:
    char_counts[s] = char_counts.get(s, 0) + 1

for s in char_counts:
    if char_counts[s] == 1:
        counts[s] = counts.get(s, 0) + 1

print(counts)

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
ten = []
x = len(numbers) - 1
y = 0

while y < len(numbers):
    if x != y:
        z = numbers[x] + numbers[y]
        if z == 10 and numbers[x] not in ten and numbers[y] not in ten:
            ten.append(numbers[x])
            ten.append(numbers[y])
    x -= 1
    y += 1

print(ten)

# 5 people object that has a blood type prop and then print the name and phone numbers of the people who match
