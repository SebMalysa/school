# Program Name:       A8P2.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 12/07/2015
# Purpose of Program: Generates a seven-digit lottery number.


import os
import random


def generate_lottery_number():

    lottery_number = [0] * 7
    number = 0

    while number < len(lottery_number):
        lottery_number[number] = random.randint(0, 9)
        number += 1

    return lottery_number


def output_lottery_number(lottery_number):

    print('\n')

    for number in lottery_number:
        print('\t\t', number)


def main():

    again = 'y'

    while again.lower() == 'y':

        lottery_number = generate_lottery_number()
        output_lottery_number(lottery_number)

        again = input('\n\t\tWould you like to generate another lottery number? (y/n): ')

        while again not in ('y', 'Y', 'n', 'N'):
            print('\n\t\t\tPlease enter y or n.\n')
            again = input('\n\t\tWould you like to generate another lottery number? (y/n): ')

        if again.lower() == 'n':
            print('\n\t\t\tGoodbye.')


main()

print('\n\n')
os.system("pause")
