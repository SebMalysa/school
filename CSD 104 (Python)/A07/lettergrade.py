# Program Name:       lettergrade.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 12/04/2015
# Purpose of Program: Stores the determine_letter_grade function


def determine_letter_grade(average_num):

    if average_num >= 80:
        average_let = 'A'

    elif average_num >= 70:
        average_let = 'B'

    elif average_num >= 60:
        average_let = 'C'

    elif average_num >= 50:
        average_let = 'D'

    else:
        average_let = 'F'

    return average_let
