# Program Name:       A7P2.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 12/04/2015
# Purpose of Program: Calculates average grade for individual students
#                     and the entire class with functions using local variables

import os
import lettergrade

student_total = 0


def input_student_data():

    name = input('\n\n\n\tEnter student name: ')
    test_1 = float(input('\n\tEnter the result of Test 1: '))
    test_2 = float(input('\n\tEnter the result of Test 2: '))
    test_3 = float(input('\n\tEnter the result of Test 3: '))

    return name, test_1, test_2, test_3


def calculate_average(test_1, test_2, test_3, test_total):

    global student_total

    average_num = (test_1 + test_2 + test_3) / 3
    test_total += average_num
    student_total += 1

    return average_num, test_total


def output_final_grade(name, average_num, average_let):

    print('\n\n\t\t\t\t\t\tSTUDENT GRADE REPORT')
    print('\n\tSTUDENT NAME\t\t\tNUMERIC AVERAGE\t\t\tLETTER GRADE')
    print('\n\t{:s}' .format(name) .ljust(36) + '{:.1f}%' .format(average_num)
          .ljust(20) + '{:s}' .format(average_let))


def calculate_class_average(test_total):

    class_ave = test_total / student_total

    return class_ave


def output_report_footers(class_ave):

    print('\n\n\tTOTAL STUDENTS PROCESSED: {:d}' .format(student_total))
    print('\tCLASS AVERAGE:' .ljust(27) + '{:.1f}%' .format(class_ave))


def main():
    again = 'y'
    test_total = 0

    while again.lower() == 'y':

        name, test_1, test_2, test_3 = input_student_data()
        average_num, test_total = calculate_average(test_1, test_2, test_3, test_total)
        average_let = lettergrade.determine_letter_grade(average_num)
        output_final_grade(name, average_num, average_let)

        again = input('\n\n\t\tWould you like to process another student? (y/n): ')

        while again not in ('y', 'Y', 'n', 'N'):
            print('\n\t\t\tPlease enter y or n.\n')
            again = input('\n\t\t\tWould you like to process another student? (y/n): ')

        if again.lower() == 'n':
            class_ave = calculate_class_average(test_total)
            output_report_footers(class_ave)
            exit()

main()

print('\n\n')
os.system('pause')
