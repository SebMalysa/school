# Program Name:       A7P1.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 12/04/2015
# Purpose of Program: Calculates salesperson commission with functions
#                     using local variables

import os
import salesperson_data


def calculate_commission(sales):

    com_rate1 = 0.05
    com_rate2 = 0.1
    com_rate3 = 0.2

    if sales <= 1000:
        com = sales * com_rate1

    elif sales <= 2000:
        com = sales * com_rate2

    else:
        com = sales * com_rate3

    return com


def determine_bonus(years_emp, com):

    bonus = 10

    if years_emp > bonus:
        com *= 2

    return com


def output_commission(name, com):

    print('\n\n\t{a:^42s}' .format(a='COMMISSION REPORT'))
    print('\n\t{a:<20s} {b:5s} {c:>17s}' .format(a='SALESPERSON NAME', b='', c='COMMISSION AMOUNT'))
    print('\n\t{a:<20s} {b:5s} ${c:>16,.2f}' .format(a=name, b='', c=com))


def main():

    again = 'y'

    while again.lower() == 'y':

        name, sales, years_emp = salesperson_data.input_salesperson_data()
        com = calculate_commission(sales)
        com = determine_bonus(years_emp, com)
        output_commission(name, com)

        again = input('\n\n\t\tWould you like to process another employee? (y/n): ')

        while again not in ('y', 'Y', 'n', 'N'):
            print('\n\t\t\tPlease enter y or n.\n')
            again = input('\n\t\t\tWould you like to process another employee? (y/n): ')

        if again.lower() == 'n':
            print('\n\t\t\tGoodbye.')


main()

print('\n\n')
os.system("pause")
