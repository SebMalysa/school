# Program Name:       salesperson_data.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 12/04/2015
# Purpose of Program: Stores the input_salesperson_data function


def input_salesperson_data():

    name = input("\n\n\n\tEnter salesperson's name: ")
    sales = float(input("\n\tEnter dollar amount of sales: "))
    years_emp = int(input("\n\tEnter years employed: "))

    return name, sales, years_emp
