# Program Name:       A5P1.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 11/11/2015
# Purpose of Program: Generates a random number between 1 - 10 and
#                     gives the user three chances to guess the number

import os
import random

print("\n\t\t\t\tNumber Guessing Game")

play = True

# Main loop for the entire program
while play:
    gus_num = 0
    ran_num = random.randint(1, 10)
    guesses = 0

# Nested loop that requests user input based on conditions to run the game with input validation
    for guesses in range(1, 4):
        while True:
            try:
                gus_num = int(input('\n\n\t\tGuess the number between 1 and 10: '))
                break
            except ValueError:
                print('\n\t\t\tPlease enter a number between 1 and 10.')
                continue

        if gus_num == ran_num:
            print('\n\t\t\tCongratulations, you got it!')
            if guesses == 1:
                print('\t\t\tYou guessed the number in %i try.\n' % guesses)
            else:
                print('\t\t\tYou guessed the number in %i tries.\n' % guesses)
            break

        if guesses == 3:
            print('\n\t\t\tSorry, you have no more guesses.')
            print('\t\t\tThe number is %i.\n' % ran_num)
        elif gus_num > ran_num:
            print('\n\t\t\tSorry, too high.\n\t\t\tTry again.')
        elif gus_num < ran_num:
            print('\n\t\t\tSorry, too low.\n\t\t\tTry again.')

# Input request to play again with validation
    while True:
        again = str(input('\t\tDo you want to play again? (y/n): '))
        if again not in ('y', 'Y', 'n', 'N'):
            print('\n\t\t\tPlease enter y or n.\n')
        elif again.lower() == 'n':
            print('\n\t\t\tGoodbye!\n')
            play = False
            break
        else:
            play = True
            break

os.system("pause")
