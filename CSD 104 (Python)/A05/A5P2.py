# Program Name:       A5P2.py
# Programmer Name:    Sebastian Malysa
# Date Last Modified: 11/11/2015
# Purpose of Program: Calculates a hotel occupancy rate based on available and
#                     occupied rooms on each floor

import os

print('\n\t\t\tHotel Occupancy Calculator')

calculate = True

# Main loop for the entire program
while calculate:
    number_floors = 0
    floor = 0
    rooms = 0
    total_rooms = 0
    occ_rooms = 0
    total_occ_rooms = 0

# Input validation for number_floors control variable
    while True:
        try:
            number_floors = int(input('\n\n\t\tNumber of Floors: '))
            break
        except ValueError:
            print('\n\t\t\tPlease enter a number of floors between 1 and 20.')
            continue
    if number_floors < 1 or number_floors > 20:
        print('\n\t\t\tPlease enter a number of floors between 1 and 20.')
        continue
    if number_floors > 3:
        number_floors += 1
    if number_floors > 12:
        number_floors += 1

# Second major loop in the program that asks for user input for available/occupied rooms on each floor
# Loops are used for input validation for each user input
    for floor in range(0, number_floors):
        floor += 1
        if floor == 4 or floor == 13:
            continue

        while True:
            try:
                rooms = int(input('\n\n\t\tNumber of rooms on floor {:d}: ' .format(floor)))
                break
            except ValueError:
                print('\n\t\t\tPlease enter a number of rooms between 1 and 10')
                continue

        while rooms < 1 or rooms > 10:
            print('\n\t\t\tPlease enter a number of rooms between 1 and 10')
            rooms = int(input('\n\t\tNumber of rooms on floor {:d}: ' .format(floor)))
            continue
        total_rooms += rooms

        while True:
            try:
                occ_rooms = int(input('\t\tNumber of occupied rooms: '))
                break
            except ValueError:
                print('\n\t\t\tPlease enter a number.\n')
                continue

        while occ_rooms < 0 or occ_rooms > rooms:
            print('\n\t\t\tOccupied rooms cannot exceed available rooms.'
                  '\n\t\t\tPlease enter a valid number.')
            occ_rooms = int(input('\n\t\tNumber of occupied rooms: '))
            continue
        total_occ_rooms += occ_rooms

# Calculation for total unoccupied rooms and occupancy rate
    total_uno_rooms = total_rooms - total_occ_rooms
    occ_rate = total_occ_rooms / total_rooms * 100

# Final print statements
    print('\n\n\t\tTotal number of rooms: {:d}' .format(total_rooms))
    print('\t\tNumber of occupied rooms: {:d}' .format(total_occ_rooms))
    print('\t\tNumber of unoccupied rooms: {:d}' .format(total_uno_rooms))
    print('\t\tOccupancy rate: {:.1f}%' .format(occ_rate))

# Input request for an additional process with input validation
    while True:
        again = str(input('\n\n\t\tWould you like to process another hotel? (y/n): '))
        if again not in ('y', 'Y', 'n', 'N'):
            print('\n\t\t\tPlease enter y or n.\n')
        elif again.lower() == 'n':
            print('\n\t\t\tGoodbye!\n')
            calculate = False
            break
        else:
            calculate = True
            break

os.system("pause")
